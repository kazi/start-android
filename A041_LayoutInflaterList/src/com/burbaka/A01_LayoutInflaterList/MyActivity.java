package com.burbaka.A01_LayoutInflaterList;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MyActivity extends Activity {
    private static final String TAG = "myLogs";
    String[] name = { "Иван", "Марья", "Петр", "Антон", "Даша", "Борис",
            "Костя", "Игорь" };
    String[] position = { "Программер", "Бухгалтер", "Программер",
            "Программер", "Бухгалтер", "Директор", "Программер", "Охранник" };
    int salary[] = { 13000, 10000, 13000, 13000, 10000, 15000, 13000, 8000 };

    int[] colors = new int[2];

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        colors[0] = Color.parseColor("#559966CC");
        colors[1] = Color.parseColor("#55336699");
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.linLayout);
        LayoutInflater layoutInflater = getLayoutInflater();

        for (int i = 0; i < name.length; i++) {
            Log.i(TAG,"i = " + i);
            View item = layoutInflater.inflate(R.layout.item, linearLayout, false);

            TextView textViewName = (TextView) item.findViewById(R.id.tvName);
            textViewName.setText(name[i]);

            TextView textViewPosition = (TextView) item.findViewById(R.id.tvPosition);
            textViewPosition.setText("Должность: " + position[i]);

            TextView textViewSalary = (TextView) item.findViewById(R.id.tvSalary);
            textViewSalary.setText("Оклад: " + String.valueOf(salary[i]));

            item.getLayoutParams().width = ViewGroup.LayoutParams.MATCH_PARENT;
            item.setBackgroundColor(colors[i % 2]);
            linearLayout.addView(item);
        }
    }
}
