package com.shapovalov.A027_GetIntentAction;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Oleksandr
 * Date: 16.01.13
 * Time: 8:05
 * To change this template use File | Settings | File Templates.
 */
public class DateAndTimeActivity extends Activity {
    String textInfo = "";
    String format = "";

    public void onCreate(Bundle savedInstanceState){
        Log.i("myLogs", "onCreate in DateAndTimeActivity was called");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.date_and_time_layout);

        Intent intent = getIntent();
        String action = intent.getAction();

        if (action.equals("ru.startandroid.intent.action.showtime")) {
            format = "HH:mm:ss";
            textInfo = "Time:";
        } else {
            format = "dd:MM:yyyy";
            textInfo = "Date:";
        }

        // в зависимости от содержимого переменной format
        // получаем дату или время в переменную datetime
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        String dateTime = simpleDateFormat.format(new Date(System.currentTimeMillis()));

        TextView textView = (TextView) findViewById(R.id.tvInfo);
        textView.setText(textInfo + " " +  dateTime);
    }
}
