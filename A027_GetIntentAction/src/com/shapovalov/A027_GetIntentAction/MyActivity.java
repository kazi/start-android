package com.shapovalov.A027_GetIntentAction;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MyActivity extends Activity implements View.OnClickListener{
    Button btnShowTime;
    Button btnShowDate;
    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        btnShowDate = (Button) findViewById(R.id.btnDate);
        btnShowTime = (Button) findViewById(R.id.btnTime);

        btnShowTime.setOnClickListener(this);
        btnShowDate.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        switch (v.getId()){
            case R.id.btnDate:
                intent = new Intent("ru.startandroid.intent.action.showdate");
            break;
            case R.id.btnTime:
                intent = new Intent("ru.startandroid.intent.action.showtime");
            break;
        }
        startActivity(intent);
    }
}
