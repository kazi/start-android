package com.Burbaka.A012_LogAndMess;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MyActivity extends Activity implements View.OnClickListener{
    private final String TAG = "myLogs";

    private Button btnOk;
    private Button btnCancel;
    private TextView textView;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        Log.i(TAG, "Initialize views");
        btnOk = (Button) findViewById(R.id.btnOk);
        btnCancel = (Button) findViewById(R.id.btnCancel);
        textView = (TextView) findViewById(R.id.tvOut);

        Log.i(TAG, "Set listeners to view");
        btnOk.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Log.i(TAG, "Get the id for the view in onClick(View v)");
        switch (v.getId()) {
            case R.id.btnOk:
                Log.i(TAG, "Ok click listener");
                textView.setText("Ok was pressed");
                Toast.makeText(this, "Ok was pressed", Toast.LENGTH_SHORT).show();
            break;
            case R.id.btnCancel:
                Log.i(TAG, "Cancel click listener");
                textView.setText("Cancel was pressed");
                Toast.makeText(this, "Cancel was pressed", Toast.LENGTH_SHORT).show();
            break;
        }
    }
}
