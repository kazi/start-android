package com.burbaka.A022_OneActivityState;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

public class MyActivity extends Activity {

    public static final String TAG = "myLogs";

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "onCreate started.");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
    }

    public void onStart(){
        super.onStart();
        Log.i(TAG, "onStart started.");
    }

    public void onResume(){
        super.onResume();
        Log.i(TAG, "onResume started.");
    }

    public void onRestart(){
        super.onRestart();
        Log.i(TAG, "onRestart started.");
    }

    public void onPause(){
        super.onPause();
        Log.i(TAG, "onPause started.");
    }

    public void onStop(){
        super.onStop();
        Log.i(TAG, "onStop started.");
    }

    public void onDestroy(){
        super.onDestroy();
        Log.i(TAG, "onDestroy started.");
    }
}
