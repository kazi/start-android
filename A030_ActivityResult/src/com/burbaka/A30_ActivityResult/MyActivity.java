package com.burbaka.A30_ActivityResult;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MyActivity extends Activity implements View.OnClickListener{
    private static final String TAG = "myLogs";
    final public int REQUEST_CODE_COLOR = 1;
    final public int REQUEST_CODE_ALIGN = 2;

    private TextView textView;
    private Button btnColor;
    private Button btnAlign;
    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i("myLogs", "onCreate in MyActivity was called");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        btnAlign = (Button) findViewById(R.id.btnAlign);
        btnColor = (Button) findViewById(R.id.btnColor);
        textView = (TextView) findViewById(R.id.tvText);

        btnColor.setOnClickListener(this);
        btnAlign.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        switch (v.getId()){
            case R.id.btnAlign:
                intent = new Intent(this,AlignActivity.class);
                startActivityForResult(intent, REQUEST_CODE_ALIGN);
            break;
            case R.id.btnColor:
                intent = new Intent(this,ColorActivity.class);
                startActivityForResult(intent, REQUEST_CODE_COLOR);
            break;
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        Log.i(TAG, "requestCode = " + requestCode + ", resultCode = " + resultCode);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_CODE_COLOR:
                    int color = data.getIntExtra("color", Color.WHITE);
                    textView.setTextColor(color);
                break;
                case REQUEST_CODE_ALIGN:
                    int align = data.getIntExtra("align", Gravity.LEFT);
                    textView.setGravity(align);
                break;
            }
        } else {
            Toast.makeText(this, "Wrong result.", Toast.LENGTH_SHORT).show();
        }
    }
}
