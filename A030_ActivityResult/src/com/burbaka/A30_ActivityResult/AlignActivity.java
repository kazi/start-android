package com.burbaka.A30_ActivityResult;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

/**
 * Created with IntelliJ IDEA.
 * User: Oleksandr
 * Date: 16.01.13
 * Time: 11:18
 */
public class AlignActivity extends Activity implements  View.OnClickListener {

    private static final String TAG = "myLogs";
    private Button btnAlignLeft;
    private Button btnAlignCenter;
    private Button btnAlignRight;

    @Override
    public void onCreate(Bundle savedInstanceState){
        Log.i(TAG, "onCreate in AlignActivity was called");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.align_activity_layout);

        btnAlignLeft = (Button) findViewById(R.id.btnLeft);
        btnAlignCenter = (Button) findViewById(R.id.btnCenter);
        btnAlignRight = (Button) findViewById(R.id.btnRight);

        btnAlignRight.setOnClickListener(this);
        btnAlignCenter.setOnClickListener(this);
        btnAlignLeft.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent();
        switch (v.getId()){
            case R.id.btnLeft:
                intent.putExtra("align", Paint.Align.LEFT);
                break;
            case R.id.btnCenter:
                intent.putExtra("align", Paint.Align.CENTER);
                break;
            case R.id.btnRight:
                intent.putExtra("align", Paint.Align.RIGHT);
                break;
        }
        setResult(RESULT_OK, intent);
        finish();
    }
}
