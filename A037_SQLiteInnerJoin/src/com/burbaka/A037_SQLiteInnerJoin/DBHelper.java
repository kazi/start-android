package com.burbaka.A037_SQLiteInnerJoin;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created with IntelliJ IDEA.
 * User: Oleksandr
 * Date: 05.02.13
 * Time: 14:51
 */
public class DBHelper extends SQLiteOpenHelper {

    //данные для таблицы должностей
    int[] positionId = {1, 2, 3, 4};
    String[] positionName = {"Директор", "Программер", "Бухгалтер", "Охранник"};
    int[] positionSalary = {15000, 13000, 10000, 8000};

    //данные для таблицы людей
    String[] peopleName = {"Иван", "Марья", "Петр", "Антон", "Даша", "Борис", "Костя", "Игорь"};
    int[] peoplePositionId = {2, 3, 2, 2, 3, 1, 2, 4};

    public DBHelper(Context context) {
        super(context, "myDB", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.i(MyActivity.TAG, "--- onCreate database ---");

        ContentValues contentValues = new ContentValues();

        // создаем таблицу должностей
        db.execSQL("create table position ("
                + "id integer primary key,"
                + "name text, "
                + "salary integer"
                + ");");

        // заполняем ее
        for (int i = 0; i < positionId.length; i++) {
            contentValues.clear();
            contentValues.put("id", positionId[i]);
            contentValues.put("name", positionName[i]);
            contentValues.put("salary", positionSalary[i]);
            db.insert("position", null, contentValues);
        }

        // создаем таблицу людей
        db.execSQL("create table people ("
                + "id integer primary key autoincrement,"
                + "name text,"
                + "posid integer"
                + ");");

        // заполняем ее
        for (int i = 0; i < peopleName.length; i++) {
            contentValues.clear();
            contentValues.put("name", peopleName[i]);
            contentValues.put("posid", peoplePositionId[i]);
            db.insert("people", null, contentValues);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
