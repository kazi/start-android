package com.burbaka.A028_IntentExtras;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MyActivity extends Activity implements View.OnClickListener{
    public static final String TAG = "myLogs";
    EditText editTextFirstName;
    EditText editTextSecondName;
    Button btnSubmit;


    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "onCreate in MyActivity was called");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        editTextFirstName = (EditText) findViewById(R.id.etFName);
        editTextSecondName = (EditText) findViewById(R.id.etLName);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);

        btnSubmit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(this, ViewActivity.class);
        intent.putExtra("firstName", editTextFirstName.getText().toString());
        intent.putExtra("secondName", editTextSecondName.getText().toString());
        startActivity(intent);
    }
}
