package com.burbaka.A028_IntentExtras;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

/**
 * Created with IntelliJ IDEA.
 * User: Oleksandr
 * Date: 16.01.13
 * Time: 9:29
 * To change this template use File | Settings | File Templates.
 */
public class ViewActivity extends Activity {

    public static final String TAG = "myLogs";
    TextView textView;

    public void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "onCreate in ViewActivity was called.");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.second_activity_view);

        textView = (TextView) findViewById(R.id.tvView);
        Intent intent = getIntent();
        String editTextFirstName = intent.getStringExtra("firstName");
        String editTextSecondName = intent.getStringExtra("secondName");

        textView.setText("Your name is: " + editTextFirstName + " " + editTextSecondName);

    }

}
