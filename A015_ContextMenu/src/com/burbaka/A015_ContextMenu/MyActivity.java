package com.burbaka.A015_ContextMenu;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class MyActivity extends Activity {
    public static final String TAG = "myLogs";

    TextView vTextColor;
    TextView vTextSize;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        vTextColor = (TextView) findViewById(R.id.tvColor);
        vTextSize = (TextView) findViewById(R.id.tvSize);

        // registering view to call context menu
        registerForContextMenu(vTextColor);
        registerForContextMenu(vTextSize);

    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        Log.i(TAG, "onCreateContextMenu was called.");
        this.getMenuInflater().inflate(R.menu.my_context_menu, menu);
        switch (v.getId()) {
            case R.id.tvColor:
                menu.setGroupVisible(R.id.menuSizeGroup, false);
                break;
            case R.id.tvSize:
                menu.setGroupVisible(R.id.menuColorGroup, false);
                break;
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        Log.i(TAG, "onContextItemSelected was called.");
        switch (item.getItemId()) {
            // menu items for tvColor
            case R.id.menuColorRed:
                vTextColor.setTextColor(Color.RED);
                vTextColor.setText("Text color = red");
                break;
            case R.id.menuColorGreen:
                vTextColor.setTextColor(Color.GREEN);
                vTextColor.setText("Text color = green");
                break;
            case R.id.menuColorBlue:
                vTextColor.setTextColor(Color.BLUE);
                vTextColor.setText("Text color = blue");
                break;
            // menu items for tvSize
            case R.id.menu_size_22:
                vTextSize.setTextSize(22);
                vTextSize.setText("Text size = 22");
                break;
            case R.id.menu_size_26:
                vTextSize.setTextSize(26);
                vTextSize.setText("Text size = 26");
                break;
            case R.id.menu_size_30:
                vTextSize.setTextSize(30);
                vTextSize.setText("Text size = 30");
                break;
        }
        return super.onContextItemSelected(item);
    }
}
