package com.burbaka.A039_SQLiteOnUpgradeDB;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created with IntelliJ IDEA.
 * User: Oleksandr
 * Date: 05.02.13
 * Time: 18:15
 */
public class DBHelper extends SQLiteOpenHelper {
    public DBHelper(Context context) {
        super(context, MyActivity.DB_NAME, null, MyActivity.DB_VERSION);
    }

    /**
     * Called when the database is created for the first time. This is where the
     * creation of tables and the initial population of the tables should happen.
     *
     * @param database The database.
     */
    @Override
    public void onCreate(SQLiteDatabase database) {
        Log.i(MyActivity.TAG, "--- onCreate database ---");

        String[] peopleName = {"Иван", "Марья", "Петр", "Антон", "Даша", "Борис", "Костя", "Игорь"};
        int[] peoplePosId = {2, 3, 2, 2, 3, 1, 2, 4};

        // данные для таблицы должностей
        int[] positionId = {1, 2, 3, 4};       ;
        String[] positionName = {"Директор", "ПРограммер", "Бухгалтер", "Охранник"};
        int[] positionSalary = {15000, 13000, 10000, 8000};

        ContentValues contentValues = new ContentValues();

        // создаем таблицу должностей
        database.execSQL("create table position ("
                + "id integer primary key,"
                + "name text, salary integer" + ");");

        // заполняем ее
        for (int i = 0; i < positionId.length; i++) {
            contentValues.clear();
            contentValues.put("id", positionId[i]);
            contentValues.put("name", positionName[i]);
            contentValues.put("salary", positionSalary[i]);
            database.insert("position", null, contentValues);
        }

        // создаем таблицу людей
        database.execSQL("create table people ("
        + "id integer primary key autoincrement,"
        + "name text, posid integer);");

        // заполняем ее
        for (int i = 0; i < peopleName.length; i++){
            contentValues.clear();
            contentValues.put("name", peopleName[i]);
            contentValues.put("posid", peoplePosId[i]);
            database.insert("people", null, contentValues);
        }
    }

    /**
     * Called when the database needs to be upgraded. The implementation
     * should use this method to drop tables, add tables, or do anything else it
     * needs to upgrade to the new schema version.
     * <p/>
     * <p>
     * The SQLite ALTER TABLE documentation can be found
     * <a href="http://sqlite.org/lang_altertable.html">here</a>. If you add new columns
     * you can use ALTER TABLE to insert them into a live table. If you rename or remove columns
     * you can use ALTER TABLE to rename the old table, then create the new table and then
     * populate the new table with the contents of the old table.
     * </p><p>
     * This method executes within a transaction.  If an exception is thrown, all changes
     * will automatically be rolled back.
     * </p>
     *
     * @param db         The database.
     * @param oldVersion The old database version.
     * @param newVersion The new database version.
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.i(MyActivity.TAG, " --- onUpgrade database from " + oldVersion + " to " + newVersion + " version --- ");

        if (oldVersion == 1 && newVersion == 2) {
            ContentValues contentValues = new ContentValues();

            // данные для таблицы должностей
            int[] positionId = {1, 2, 3, 4};
            String[] positionName = {"Директор", "Программер", "Бухгалтер", "Охранник"};
            int[] positionSalary = {15000, 13000, 10000, 8000};

            try {
                db.beginTransaction();

                // создаем таблицу должностей
                db.execSQL("create table position ("
                        + "id integer primary key,"
                        + "name text, salary integer);");

                // заполняем ее
                for (int i = 0; i < positionId.length; i++) {
                    contentValues.clear();
                    contentValues.put("id", positionId[i]);
                    contentValues.put("name", positionName[i]);
                    contentValues.put("salary", positionSalary[i]);
                    db.insert("position", null, contentValues);
                }

                db.execSQL("alter table people add column posid integer;");

                for (int i = 0; i < positionId.length; i++) {
                    contentValues.clear();
                    contentValues.put("posid", positionId[i]);
                    db.update("people", contentValues, "position = ?", new String[]{positionName[i]});
                }

                db.execSQL("create temporary table people_tmp("
                        + "id integer, name text, position text, posid integer);");

                db.execSQL("insert into people_tmp select id, name, position, posid from people;");
                db.execSQL("drop table people;");

                db.execSQL("create table people ("
                        + "id integer primary key autoincrement,"
                        + "name text, posid integer);");

                db.execSQL("insert into people select id, name, posid from people_tmp;");
                db.execSQL("drop table people_tmp;");

                db.setTransactionSuccessful();
            } finally {
                db.endTransaction();
            }
        }

    }
}
