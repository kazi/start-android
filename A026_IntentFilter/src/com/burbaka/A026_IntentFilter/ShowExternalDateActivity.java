package com.burbaka.A026_IntentFilter;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Oleksandr
 * Date: 16.01.13
 * Time: 7:21
 * To change this template use File | Settings | File Templates.
 */
public class ShowExternalDateActivity extends Activity {

    final private String TAG = "myLogs";

    @Override
    public void onCreate(Bundle savedInstanceState){

        Log.i(TAG, "onCreate in ShowExternalDateActicvity started.");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show_date_layout);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE, MMM d, yyyy");
        String date = simpleDateFormat.format(new Date(System.currentTimeMillis()));

        TextView textView = (TextView) findViewById(R.id.tvDate);
        textView.setText(date);
    }
}
