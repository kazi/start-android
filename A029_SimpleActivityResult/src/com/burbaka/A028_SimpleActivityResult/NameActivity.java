package com.burbaka.A028_SimpleActivityResult;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created with IntelliJ IDEA.
 * User: Oleksandr
 * Date: 16.01.13
 * Time: 10:02
 * To change this template use File | Settings | File Templates.
 */
public class NameActivity extends Activity implements View.OnClickListener{
    private Button btnOk;
    private EditText editText;

    public void onCreate(Bundle savedInstanceState){
        Log.i("myLogs", "onCreate in NameActivity was called");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.name_activity_layout);

        btnOk = (Button) findViewById(R.id.btnOK);
        editText = (EditText) findViewById(R.id.etName);
        btnOk.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent();
        intent.putExtra("name", editText.getText().toString());
        setResult(RESULT_OK, intent);
        finish();

    }

}
