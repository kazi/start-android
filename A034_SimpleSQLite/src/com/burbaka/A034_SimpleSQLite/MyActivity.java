package com.burbaka.A034_SimpleSQLite;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteCantOpenDatabaseException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MyActivity extends Activity implements View.OnClickListener{
    final private String TAG = "myLogs";

    private Button btnAdd;
    private Button btnRead;
    private Button btnClear;

    EditText editTextName;
    EditText editTextEmail;

    DBHelper dbHelper;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "obCreate in MyActivity started.");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        dbHelper = new DBHelper(this);

        btnAdd = (Button) findViewById(R.id.btnAdd);
        btnClear = (Button) findViewById(R.id.btnClear);
        btnRead = (Button) findViewById(R.id.btnRead);

        editTextEmail = (EditText) findViewById(R.id.etEmail);
        editTextName = (EditText) findViewById(R.id.etName);

        btnRead.setOnClickListener(this);
        btnClear.setOnClickListener(this);
        btnAdd.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Log.i(TAG, "onClick in MyActivity started.");
        // создаем объект для данных
        ContentValues contentValues = new ContentValues();

        // получаем данные из полей ввода
        String name = editTextName.getText().toString();
        String email = editTextEmail.getText().toString();

        // подключаемся к БД
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        Log.d(TAG, "Перед switch в MyActivity.");
        switch (v.getId()){
            case R.id.btnAdd:
                Log.i(TAG, "--- Insert in mytable: ---");
                // подготовим данные для вставки в виде пар: наименование столбца - значение
                contentValues.put("name", name);
                contentValues.put("email", email);

                // вставляем запись и получаем ее ID
                long  rowID = db.insert("mytable", null, contentValues);
                Log.i(TAG, "row inserted, ID = " + rowID);
            break;
            case R.id.btnRead:
                Log.i(TAG, "--- Rows in mytable: ---");
                // делаем запрос всех данных из таблицы mytable, получаем Cursor
                Cursor cursor = db.query("mytable", null, null, null, null, null, null);

                // ставим позицию курсора на первую строку выборки
                // если в выборке нет строк, вернется false
                if (cursor.moveToFirst()) {
                    // определяем номера столбцов по имени в выборке
                    int idColumnIndex = cursor.getColumnIndex("id");
                    int nameColumnIndex = cursor.getColumnIndex("name");
                    int emailColumnIndex = cursor.getColumnIndex("email");

                    do {
                        // получаем значения по номерам столбцов и пишем все в лог
                        Log.i(TAG, "id = " + cursor.getInt(idColumnIndex) + ", name = " + cursor.getString(nameColumnIndex) + ", email = " + cursor.getString(emailColumnIndex));
                    } while (cursor.moveToNext());
                } else {
                    Log.i(TAG, "0 rows.");
                }
                cursor.close();
            break;
            case R.id.btnClear:
                Log.i(TAG, "--- Clear mytable: ---");
                // удаляем все записи
                int clearCount = db.delete("mytable", null, null);
                Log.i(TAG, "deleted rows count = " + clearCount);
            break;
        }

        // закрываем подключение к БД
        dbHelper.close();
    }
}
