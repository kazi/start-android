package com.burbaka.A036_SQLiteQuery;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;

public class MyActivity extends Activity implements View.OnClickListener{
    final private String TAG = "myLogs";

    String name[] = {"Китай", "США", "Бразилия", "Россия", "Япония",
            "Германия", "Египет", "Италия", "Франция", "Канада"};
    int people[] = {1400, 311, 195, 142, 128, 82, 80, 60, 66, 35};
    String region[] = {"Азия", "Америка", "Америка", "Европа", "Азия",
            "Европа", "Африка", "Европа", "Европа", "Америка"};

    private Button btnAll;
    private Button btnFunc;
    private Button btnPeople;
    private Button btnSort;
    private Button btnGroup;
    private Button btnHaving;

    private EditText editTextFunc;
    private EditText editTextPeople;
    private EditText editTextRegionPeople;

    private RadioGroup radioGroupSort;

    DBHelper dbHelper;
    SQLiteDatabase db;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        btnAll = (Button) findViewById(R.id.btnAll);
        btnFunc = (Button) findViewById(R.id.btnFunc);
        btnGroup = (Button) findViewById(R.id.btnGroup);
        btnHaving = (Button) findViewById(R.id.btnHaving);
        btnPeople = (Button) findViewById(R.id.btnPeople);
        btnSort = (Button) findViewById(R.id.btnSort);

        btnAll.setOnClickListener(this);
        btnFunc.setOnClickListener(this);
        btnGroup.setOnClickListener(this);
        btnHaving.setOnClickListener(this);
        btnPeople.setOnClickListener(this);
        btnSort.setOnClickListener(this);

        editTextFunc = (EditText) findViewById(R.id.etFunc);
        editTextPeople = (EditText) findViewById(R.id.etPeople);
        editTextRegionPeople = (EditText) findViewById(R.id.etRegionPeople);

        radioGroupSort = (RadioGroup) findViewById(R.id.rgSort);

        dbHelper = new DBHelper(this);
        // подключаемся к базе
        db = dbHelper.getWritableDatabase();

        Cursor cursor = db.query("mytable", null, null, null, null, null, null);

        if (cursor.getCount() == 0) {
            Log.i(TAG, "Database is empty");
            ContentValues contentValues = new ContentValues();
            // заполним таблицу
            for (int i = 0; i<10; i++) {
                contentValues.put("name", name[i]);
                contentValues.put("people", people[i]);
                contentValues.put("region", region[i]);
                Log.i(TAG, "id = " + db.insert("mytable", null, contentValues));
            }
        }
        cursor.close();
        dbHelper.close();
        // эмулируем нажатие кнопки btnAll
        onClick(btnAll);
    }

    @Override
    public void onClick(View v) {
        // подключаемся к базе
        db = dbHelper.getWritableDatabase();

        // данные с экрана
        String sFunc = editTextFunc.getText().toString();
        String sPeople = editTextPeople.getText().toString();
        String sRegionPeople = editTextRegionPeople.getText().toString();

        // переменные для query
        String[] columns = null;
        String selection = null;
        String[] selectionArgs = null;
        String groupBy = null;
        String having = null;
        String orderBy = null;

        // курсор
        Cursor cursor = null;

        // определяем нажатую кнопку
        switch (v.getId()) {
            // Все записи
            case R.id.btnAll:
                Log.i(TAG, "--- Все записи ---");
                cursor = db.query("mytable", null, null, null, null, null, null);
            break;
            // Функция
            case R.id.btnFunc:
                Log.i(TAG, "--- Функция " + sFunc + " ---");
                columns = new String[] {sFunc};
                cursor = db.query("mytable", columns, null, null, null, null, null);
                break;
            // Население больше чем
            case R.id.btnPeople:
                Log.d(TAG, "--- Население больше " + sPeople + " ---");
                selection = "people > ?";
                selectionArgs = new String[] {sPeople};
                cursor = db.query("mytable", null, selection, selectionArgs, null, null, null);
            break;
            // Население по региону
            case R.id.btnGroup:
                Log.i(TAG, "--- Население по региону ---");
                columns = new String[]{"region", "sum(people) as people" };
                groupBy = "region";
                cursor = db.query("mytable", columns, null, null, groupBy, null, null);
            break;
            // Население по региону больше чем
            case R.id.btnHaving:
                Log.i(TAG, "--- Регионы с населением больше " + sRegionPeople + " ---");
                columns = new String[] {"region", "sum(people) as people"};
                groupBy = "region";
                having = "sum(people) > " + sRegionPeople;
                cursor = db.query("mytable", columns, null, null, groupBy, having, null);
                break;
            // Сортировка
            case R.id.btnSort:
                // сортировка по
                switch (radioGroupSort.getCheckedRadioButtonId()){
                    // наименование
                    case (R.id.rName):
                        Log.i(TAG, "--- Сортировка по наименованию ---");
                        orderBy = "name";
                    break;
                    // население
                    case (R.id.rPeople):
                        Log.i(TAG, "--- Сортировка по населению ---");
                        orderBy = "people";
                    break;
                    case (R.id.rRegion):
                        Log.i(TAG, "--- Сортировка по населению ---");
                        orderBy = "region";
                    break;
                }
                cursor = db.query("mytable", null, null, null, null ,null, orderBy);
            break;
        }

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                String str;
                do {
                    str = "";
                    for (String cn : cursor.getColumnNames()) {
                        str = str.concat(cn + " = " + cursor.getString(cursor.getColumnIndex(cn))+";");
                    }
                    Log.i(TAG, str);
                } while (cursor.moveToNext());
            }
            cursor.close();
        } else {
            Log.i(TAG, "Cursor is null");
        }
        dbHelper.close();
    }
}
