package com.burbaka.A013_MenuSimple;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class MyActivity extends Activity {
    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
    }

    public boolean onCreateOptionsMenu(Menu menu){
        menu.add("menu 1");
        menu.add("menu 2");
        menu.add("menu 3");
        menu.add("menu 4");

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        Toast.makeText(this, "Was pressed " + item.getTitle() + ".", Toast.LENGTH_SHORT).show();
        return super.onOptionsItemSelected(item);
    }
}
