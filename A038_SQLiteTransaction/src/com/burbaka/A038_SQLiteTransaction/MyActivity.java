package com.burbaka.A038_SQLiteTransaction;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.DropBoxManager;
import android.util.Log;

public class MyActivity extends Activity {
    final public static String TAG = "myLogs";

    DBHelper dbHelper;
    SQLiteDatabase sqLiteDatabase;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        Log.d(TAG, "--- onCreate Activity ---");
        dbHelper = new DBHelper(this);
        myActions();
    }

    private void myActions() {
        SQLiteDatabase db1 = dbHelper.getWritableDatabase();
        SQLiteDatabase db2 = dbHelper.getWritableDatabase();
        Log.i(TAG, "db1 = db2 - " + db1.equals(db2));
        Log.i(TAG, "db1 open - " + db1.isOpen() + ", db2 open - " + db2.isOpen());
        db2.close();
        Log.i(TAG, "db1 open - " + db1.isOpen() + ", db2 open - " + db2.isOpen());
        dbHelper.close();


        try {
            sqLiteDatabase = dbHelper.getWritableDatabase();
            delete(sqLiteDatabase, "mytable");
            sqLiteDatabase.beginTransaction();
            insert(sqLiteDatabase, "mytable", "val1");

            Log.i(TAG, "create DBHelper");
            DBHelper dbH = new DBHelper(this);
            Log.i(TAG, "get database");
            SQLiteDatabase db = dbH.getWritableDatabase();
            read(db, "mytable");
            dbH.close();

            sqLiteDatabase.setTransactionSuccessful();
            sqLiteDatabase.endTransaction();

            read(sqLiteDatabase, "mytable");
            dbHelper.close();
        } catch (Exception ex) {
            Log.i(TAG, ex.getClass() + " error: " + ex.getMessage());
        }
//        sqLiteDatabase.setTransactionSuccessful();
//        insert(sqLiteDatabase, "mytable", "val2");
//        sqLiteDatabase.endTransaction();
//        insert(sqLiteDatabase, "mytable", "val3");
//        read(sqLiteDatabase, "mytable");
//        dbHelper.close();
    }

    void insert (SQLiteDatabase database, String table, String value) {
        Log.d(TAG, "Insert in table " + table + " value = " + value);
        ContentValues contentValues = new ContentValues();
        contentValues.put("val", value);
        sqLiteDatabase.insert(table, null, contentValues);
    }

    void read(SQLiteDatabase database, String table) {
        Log.i(TAG, "Read table " + table);
        Cursor cursor = sqLiteDatabase.query(table, null, null, null, null, null, null);
        if (cursor != null) {
            Log.i(TAG, "Records count = " + cursor.getCount());
            if (cursor.moveToFirst()){
                do {
                    Log.i(TAG, cursor.getString(cursor.getColumnIndex("val")));
                } while (cursor.moveToNext());
            } else {
                Log.i(TAG, "Database is empty");
            }
            cursor.close();
        } else {
            Log.i(TAG, "Cursor is null");
        }
    }

    void delete(SQLiteDatabase database, String table) {
        Log.i(TAG, "Delete all from table " + table);
        database.delete(table, null, null);
    }
}
